# ZSSN-API BACKEND #

* This is the project of the back-end for the ZSSN(Zombie Survivor Social Network) using Java and Spring Framework to provide a REST API.
* As a maven project all the dependencies are declared in the file pom.xml of the project.

##Required##

* JDK 1.8
* Apache Tomcat 8.0.1
* MySql 5.*
* Preference for IDE: Eclipse Mars 2

## Database configuration

* You should have the MySql version installed and some cliente to run the scripts.
* The scripts are in the "src/database" folder. 

### Contribution guidelines ###

* Architecture definition
* Development
* Code review
* Tests

### Who do I talk to? ###

* Fernando Gualberto (e-mail: fernando.gmp@gmail.com / Cel.: (62) 9 9292-4386)