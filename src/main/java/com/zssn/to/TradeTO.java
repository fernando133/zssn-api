package com.zssn.to;

import java.util.List;

import com.zssn.model.Inventory;
import com.zssn.model.Survivor;

/**
 * 
 * @author fernandogualberto
 *
 */
public class TradeTO {
	
	private Survivor firstSurvivor;
	private Survivor secondSurvivor;
	private List<Inventory> firstSurvivorItens;
	private List<Inventory> secondSurvivorItens;
	
	public TradeTO() {
		
	}

	public Survivor getFirstSurvivor() {
		return firstSurvivor;
	}

	public void setFirstSurvivor(Survivor firstSurvivor) {
		this.firstSurvivor = firstSurvivor;
	}

	public Survivor getSecondSurvivor() {
		return secondSurvivor;
	}

	public void setSecondSurvivor(Survivor secondSurvivor) {
		this.secondSurvivor = secondSurvivor;
	}

	public List<Inventory> getFirstSurvivorItens() {
		return firstSurvivorItens;
	}

	public void setFirstSurvivorItens(List<Inventory> firstSurvivorItens) {
		this.firstSurvivorItens = firstSurvivorItens;
	}

	public List<Inventory> getSecondSurvivorItens() {
		return secondSurvivorItens;
	}

	public void setSecondSurvivorItens(List<Inventory> secondSurvivorItens) {
		this.secondSurvivorItens = secondSurvivorItens;
	}
}
