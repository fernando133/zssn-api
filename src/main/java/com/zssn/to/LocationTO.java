package com.zssn.to;

import com.zssn.model.Survivor;

/**
 * 
 * @author fernandogualberto
 *
 */
public class LocationTO {

	private Survivor survivor;
	private double lat;
	private double lon;

	public LocationTO() {

	}

	public Survivor getSurvivor() {
		return survivor;
	}

	public void setSurvivor(Survivor survivor) {
		this.survivor = survivor;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

}
