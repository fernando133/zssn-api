package com.zssn.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.zssn.dao.SurvivorDao;
import com.zssn.model.Survivor;
import com.zssn.to.LocationTO;

/**
 * Class with the business rules of {@link Survivor}
 * 
 * @author fernandogualberto
 *
 */
public class SurvivorServicesImpl implements SurvivorServices {

	@Autowired
	SurvivorDao survivorDao;

	public boolean addEntity(Survivor survivor) throws Exception {
		return survivorDao.addEntity(survivor);
	}

	public boolean updateEntity(Survivor survivor) throws Exception {
		return survivorDao.updateEntity(survivor);
	}

	public Survivor getEntityById(long id) throws Exception {
		return survivorDao.getEntityById(id);
	}

	public List<Survivor> getEntityList() throws Exception {
		return survivorDao.getEntityList();
	}

	public boolean deleteEntity(long id) throws Exception {
		return survivorDao.deleteEntity(id);
	}

	/**
	 * Mark a given {@link Survivor} as infected.
	 * 
	 * @param survivor
	 * @throws Exception
	 */
	public void markAsInfected(Survivor survivor) throws Exception {
		int timesMarkedAsInfected = survivor.getTimesMarkedAsInfected();
		if (survivor.getTimesMarkedAsInfected() < 3) {
			survivor.setTimesMarkedAsInfected(timesMarkedAsInfected + 1);
		}
		if (survivor.getTimesMarkedAsInfected() >= 3) {
			survivor.setInfected(Boolean.TRUE);
		}
		
		survivor.setUpdatedAt(new Date());
		survivorDao.updateEntity(survivor);
	}
	
	/**
	 * Update the location of the survivor with the last latitude, longitude
	 * informed.
	 * 
	 * @param newLocation
	 * @throws Exception
	 */
	public void updateLocation(LocationTO newLocation) throws Exception {
		newLocation.getSurvivor().setLat(newLocation.getLat());
		newLocation.getSurvivor().setLon(newLocation.getLon());
		newLocation.getSurvivor().setUpdatedAt(new Date());
		survivorDao.updateEntity(newLocation.getSurvivor());
	}

}
