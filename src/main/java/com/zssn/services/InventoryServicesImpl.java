package com.zssn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.zssn.dao.InventoryDao;
import com.zssn.model.Inventory;
import com.zssn.model.Message;
import com.zssn.model.Status;
import com.zssn.model.Survivor;
import com.zssn.to.TradeTO;

/**
 * Class with business rules of {@Link Inventory}
 * 
 * @author fernandogualberto
 *
 */
public class InventoryServicesImpl implements InventoryServices {

	@Autowired
	InventoryDao inventoryDao;

	public boolean addEntity(Inventory inventory) throws Exception {
		return inventoryDao.addEntity(inventory);
	}

	public boolean updateEntity(Inventory inventory) throws Exception {
		return inventoryDao.updateEntity(inventory);
	}

	public Inventory getEntityById(long id) throws Exception {
		return inventoryDao.getEntityById(id);
	}

	public List<Inventory> getEntityList() throws Exception {
		return inventoryDao.getEntityList();
	}

	public boolean deleteEntity(long id) throws Exception {
		return inventoryDao.deleteEntity(id);
	}

	/**
	 * Add a list of inventory for one {@link Survivor}.
	 * 
	 * @param listInventory
	 * @return
	 * @throws Exception
	 */
	public void addListInventory(final List<Inventory> listInventory) throws Exception {
		for (Inventory inventory : listInventory) {
			inventoryDao.addEntity(inventory);
		}
	}
	
	/**
	 * Get all inventory from a specific survivor by a given survivorId.
	 * 
	 * @param survivor
	 * @throws Exception
	 */
	public List<Inventory> getBySurvivor(final long survivorId) throws Exception {
		return inventoryDao.getBySurvivor(survivorId);
	}

	/**
	 * Make a trade of itens between to distinct survivors.
	 * 
	 * @param trade
	 * @return
	 * @throws Exception 
	 */
	public Status makeTradeBetweenSurvivors(TradeTO trade) {
		try {
			if (validadePointsToTrade(trade)) {
				decreaseInventory(trade.getFirstSurvivor(), trade.getFirstSurvivorItens());
				decreaseInventory(trade.getSecondSurvivor(), trade.getSecondSurvivorItens());
				increaseInventory(trade.getFirstSurvivor(), trade.getSecondSurvivorItens());
				increaseInventory(trade.getSecondSurvivor(), trade.getFirstSurvivorItens());
				return new Status(1, Message.TRADE_MADE_SUCCESS);
			} else {
				return new Status(0, Message.POINTS_DOESNT_MATCH);
			}
		} catch (Exception e) {
			return new Status(0, e.toString());
		}
	}
	
	/**
	 * Decrease the number of itens of the survivor inventory.
	 * 
	 * @param survivor
	 * @param survivorItens
	 * @return
	 * @throws Exception 
	 */
	private void decreaseInventory(final Survivor survivor, final List<Inventory> survivorItens) throws Exception {
		for (Inventory inventory : survivorItens) {
			Inventory inventoryUpdate = inventoryDao.getBySurvivorAndItem(survivor.getId(), inventory.getItem().getId());
			inventoryUpdate.setAmount(inventoryUpdate.getAmount() - inventory.getAmount());
			inventoryDao.updateEntity(inventoryUpdate);
		}
	}

	/**
	 * Increase the inventory of survivor with the amount of new itens.
	 * 
	 * @param survivor
	 * @param survivorItens
	 * @return
	 * @throws Exception
	 */
	private void increaseInventory(final Survivor survivor, final List<Inventory> survivorItens) throws Exception {
		for (Inventory inventory : survivorItens) {
			Inventory inventoryUpdate = inventoryDao.getBySurvivorAndItem(survivor.getId(),inventory.getItem().getId());
			inventoryUpdate.setAmount(inventoryUpdate.getAmount() + inventory.getAmount());
			inventoryDao.updateEntity(inventoryUpdate);
		}
	}
	
	/**
	 * Validate if the sum of points of the list of the two survivors are the
	 * same.
	 * 
	 * @param trade
	 * @return
	 */
	private boolean validadePointsToTrade(TradeTO trade) {

		int sumPointsItensFirstSurvivor = 0;
		int sumPointsItensSecondSurvivor = 0;

		for (Inventory inventory : trade.getFirstSurvivorItens()) {
			if(inventory.getAmount() > 0) {
				sumPointsItensFirstSurvivor += inventory.getItem().getPoints() * inventory.getAmount();
			}
		}

		for (Inventory inventory2 : trade.getSecondSurvivorItens()) {
			if(inventory2.getAmount() > 0) {
			sumPointsItensSecondSurvivor += inventory2.getItem().getPoints() * inventory2.getAmount();
			}
		}

		return sumPointsItensFirstSurvivor == sumPointsItensSecondSurvivor;
	}
}
