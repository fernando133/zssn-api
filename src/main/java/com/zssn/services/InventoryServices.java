package com.zssn.services;

import java.util.List;
import com.zssn.model.Inventory;
import com.zssn.model.Status;
import com.zssn.to.TradeTO;

/**
 * 
 * @author fernandogualberto
 *
 */
public interface InventoryServices {

	public boolean addEntity(Inventory Inventory) throws Exception;

	public boolean updateEntity(Inventory Inventory) throws Exception;

	public Inventory getEntityById(long id) throws Exception;

	public List<Inventory> getEntityList() throws Exception;

	public boolean deleteEntity(long id) throws Exception;

	public void addListInventory(List<Inventory> listInventory) throws Exception;
	
	public List<Inventory> getBySurvivor(final long survivorId) throws Exception;
	
	public Status makeTradeBetweenSurvivors(TradeTO trade) throws Exception;
}
