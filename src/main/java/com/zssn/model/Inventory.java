package com.zssn.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Model class for Inventory.
 * 
 * @author fernandogualberto
 *
 */
@Entity
@Table(name = "inventory")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Inventory implements Serializable {

	private static final long serialVersionUID = 4960788808869128480L;
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "idSurvivor")
	private Survivor survivor;
	
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn(name = "idItem", nullable = false)
	private Item item;
	
	@Column(name="amount")
	private int amount;
	
	public Inventory() {
		
	}

	/**
	 * Constructor of the class.
	 * 
	 * @param survivor
	 * @param item
	 * @param amount
	 */
	public Inventory(Survivor survivor, Item item, int amount) {
		this.survivor = survivor;
		this.item = item;
		this.amount = amount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public Survivor getSurvivor() {
		return survivor;
	}

	public void setSurvivor(Survivor survivor) {
		this.survivor = survivor;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
