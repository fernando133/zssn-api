package com.zssn.model;

/**
 * Class to provide messages.
 * @author fernandogualberto
 *
 */
public class Message {
	public static final String SURVIVOR_ADDED_SUCCESS = "Survivor added Successfully!";
	public static final String INVENTORY_ADDED_SUCCESS = "List of Inventory added Succefully";
	public static final String SURVIVOR_MARKED_INFECTED_SUCCESS = "Survivor marked as infected successfully!";
	public static final String LOCATION_UPDATED_SUCCESS = "Location uptade successfully!";
	public static final String TRADE_MADE_SUCCESS = "Trade made successfully!";
	public static final String POINTS_DOESNT_MATCH ="The sum of points of the two parts has to be the same!";
}
