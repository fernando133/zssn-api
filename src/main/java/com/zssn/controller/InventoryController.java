package com.zssn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zssn.model.Inventory;
import com.zssn.model.Message;
import com.zssn.model.Status;
import com.zssn.model.Survivor;
import com.zssn.services.InventoryServices;
import com.zssn.to.TradeTO;
/**
 * Controller class for the {@link Inventory}.
 * 
 * @author fernandogualberto
 *
 */
@Controller
@RequestMapping("/inventory")
public class InventoryController {
	
	@Autowired
	InventoryServices inventoryServices;
	
	/**
	 * Add a new list of {@link Inventory}.
	 * 
	 * @param listInventory
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status addInventory(@RequestBody List<Inventory> listInventory) {
		try {
			inventoryServices.addListInventory(listInventory);
			return new Status(1, Message.INVENTORY_ADDED_SUCCESS);
		} catch (Exception e) {
			return new Status(0, e.toString());
		}
	}	
	
	/**
	 * Get list {@link Inventory} by an specific {@link Survivor} informed.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{survivorId}", method = RequestMethod.GET)
	public @ResponseBody List<Inventory> getBySurvivor(@PathVariable("survivorId") long survivorId) {
		List<Inventory> listInventory = null;
		try {
			listInventory = inventoryServices.getBySurvivor(survivorId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listInventory;
	}
	
	/**
	 * Make a trade between survivors.
	 * 
	 * @param trade
	 * @return
	 */
	@RequestMapping(value = "/makeTrade", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status makeTradeBetweenSurvivors(@RequestBody TradeTO trade) {
		try {
			Status status = inventoryServices.makeTradeBetweenSurvivors(trade);
			return status;
		} catch (Exception e) {
			return new Status(0, e.toString());
		}
	}	
}
