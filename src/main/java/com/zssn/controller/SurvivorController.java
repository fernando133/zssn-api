package com.zssn.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zssn.model.Message;
import com.zssn.model.Status;
import com.zssn.model.Survivor;
import com.zssn.services.SurvivorServicesImpl;
import com.zssn.to.LocationTO;

/**
 * Controller class for the {@link Survivor}.
 * 
 * @author fernandogualberto
 *
 */
@Controller
@RequestMapping("/survivor")
public class SurvivorController {

	@Autowired
	SurvivorServicesImpl survivorServices;

	static final Logger logger = Logger.getLogger(SurvivorController.class);
	
	/**
	 * Create a new {@link Survivor}.
	 * 
	 * @param survivor
	 * @return
	 */
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status addSurvivor(@RequestBody Survivor survivor) {
		try {
			survivor.setCreatedAt(new Date());
			survivorServices.addEntity(survivor);
			return new Status(1, Message.SURVIVOR_ADDED_SUCCESS);
		} catch (Exception e) {
			return new Status(0, e.toString());
		}
	}

	/**
	 * Get a list of all survivors.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody List<Survivor> getSurvivor() {
		List<Survivor> survivorList = new ArrayList<Survivor>();
		try {
			survivorList = survivorServices.getEntityList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return survivorList;
	}

	/**
	 * Mark a {@link Survivor} as infected.
	 * 
	 * @param survivor
	 * @return
	 */
	@RequestMapping(value = "/markAsInfected", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status markAsInfected(@RequestBody Survivor survivor) {
		try {
			survivorServices.markAsInfected(survivor);
			return new Status(1, Message.SURVIVOR_MARKED_INFECTED_SUCCESS);
		} catch (Exception e) {
			return new Status(0, e.toString());
		}
	}

	/**
	 * Get a {@link Survivor} by an specific id informed.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Survivor getSurvivor(@PathVariable("id") long id) {
		Survivor survivor = null;
		try {
			survivor = survivorServices.getEntityById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return survivor;
	}
	
	/**
	 * Update the last location of a {@link Survivor}.
	 * 
	 * @param survivor
	 * @return
	 */
	@RequestMapping(value = "/updateLocation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status markAsInfected(@RequestBody LocationTO newLocation) {
		try {
			survivorServices.updateLocation(newLocation);
			return new Status(1, Message.LOCATION_UPDATED_SUCCESS);
		} catch (Exception e) {
			return new Status(0, e.toString());
		}
	}
}
