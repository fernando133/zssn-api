package com.zssn.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.zssn.model.Inventory;

/**
 * Data access object implementation for the {@link Inventory}.
 * 
 * @author fernandogualberto
 *
 */
public class InventoryDaoImpl implements InventoryDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;

	public boolean addEntity(Inventory inventory) throws Exception {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(inventory);
			tx.commit();
			session.close();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			return true;
		}
	}

	public boolean updateEntity(Inventory inventory) throws Exception {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(inventory);
			tx.commit();
			session.close();
			return false;
		} catch (Exception e) {
			tx.rollback();
			return true;
		}
	}

	public Inventory getEntityById(long id) throws Exception {
		try {
			session = sessionFactory.openSession();
			Inventory inventory = (Inventory) session.load(Inventory.class, new Long(id));
			tx = session.getTransaction();
			session.beginTransaction();
			tx.commit();
			return inventory;
		} catch (Exception e) {
			tx.rollback();
			return null;
		}
	}

	public List<Inventory> getEntityList() throws Exception {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Inventory> inventoryList = session.createCriteria(Inventory.class).list();
			tx.commit();
			session.close();
			return inventoryList;
		} catch (Exception e) {
			tx.rollback();
			return null;
		}
	}

	public boolean deleteEntity(long id) throws Exception {
		try {
			session = sessionFactory.openSession();
			Object o = session.load(Inventory.class, id);
			tx = session.getTransaction();
			session.beginTransaction();
			session.delete(o);
			tx.commit();
			return false;
		} catch (Exception e) {
			tx.rollback();
			return true;
		}
	}

	/**
	 * Get all {@link Inventory} from a given survivor id.
	 * 
	 * @param survivor
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Inventory> getBySurvivor(final long survivorId) {
		
		String sql = "SELECT inventory FROM com.zssn.model.Inventory inventory "
				   + "INNER JOIN FETCH inventory.item item " 
				   + "INNER JOIN FETCH inventory.survivor survivor "
				   + "WHERE survivor.id = :survivorId " 
				   + "ORDER BY item.id ASC ";
		
		try {
			session = sessionFactory.openSession();
			tx = session.getTransaction();
			session.beginTransaction();
			List<Inventory> list = new ArrayList<Inventory>();
			list = session.createQuery(sql).setParameter("survivorId", survivorId).list();
			tx.commit();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return null;
	}
	
	/**
	 * Get an specific {@link Inventory} by a survivorId and intemId informed.
	 * 
	 * @param survivorId
	 * @param itemId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Inventory getBySurvivorAndItem(final long survivorId, final long itemId) {
		
		String sql = "SELECT inventory FROM com.zssn.model.Inventory inventory "
				   + "INNER JOIN FETCH inventory.item item " 
				   + "INNER JOIN FETCH inventory.survivor survivor "
				   + "WHERE survivor.id = :survivorId "
				   + "AND item.id = :itemId ";
				   
		try {
			session = sessionFactory.openSession();
			tx = session.getTransaction();
			session.beginTransaction();
			List<Inventory> list = new ArrayList<Inventory>();
			list = session.createQuery(sql).setParameter("survivorId", survivorId).setParameter("itemId", itemId).list();
			tx.commit();
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		}
		return null;
	}
}
