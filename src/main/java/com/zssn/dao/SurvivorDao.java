package com.zssn.dao;

import java.util.List;

import com.zssn.model.Survivor;

public interface SurvivorDao {

	public boolean addEntity(Survivor survivor) throws Exception;

	public boolean updateEntity(Survivor survivor) throws Exception;

	public Survivor getEntityById(long id) throws Exception;

	public List<Survivor> getEntityList() throws Exception;

	public boolean deleteEntity(long id) throws Exception;

}
