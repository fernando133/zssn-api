package com.zssn.dao;

import java.util.List;

import com.zssn.model.Inventory;

public interface InventoryDao {

	public boolean addEntity(Inventory inventory) throws Exception;

	public boolean updateEntity(Inventory inventory) throws Exception;

	public Inventory getEntityById(long id) throws Exception;

	public List<Inventory> getEntityList() throws Exception;

	public boolean deleteEntity(long id) throws Exception;
	
	public List<Inventory> getBySurvivor(final long survivorId) throws Exception;
	
	public Inventory getBySurvivorAndItem(final long survivorId, final long itemId) throws Exception;

}
