package com.zssn.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.zssn.model.Survivor;

/**
 * Data access object implementation for the {@link Survivor}.
 * 
 * @author fernandogualberto
 *
 */
public class SurvivorDaoImpl implements SurvivorDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;

	public boolean addEntity(Survivor survivor) throws Exception {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(survivor);
			tx.commit();
			session.close();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			return true;
		}
	}

	public boolean updateEntity(Survivor survivor) throws Exception {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.update(survivor);
			tx.commit();
			session.close();
			return false;
		} catch (Exception e) {
			tx.rollback();
			return true;
		}
	}

	public Survivor getEntityById(long id) throws Exception {
		try {
			session = sessionFactory.openSession();
			Survivor survivor = new Survivor();
			survivor = (Survivor)session.load(Survivor.class, new Long(id));
			tx = session.getTransaction();
			session.beginTransaction();
			tx.commit();
			return survivor;
		} catch (Exception e) {
			tx.rollback();
			return null;
		}
	}

	public List<Survivor> getEntityList() throws Exception {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Survivor> survivorList = session.createCriteria(Survivor.class).list();
			tx.commit();
			session.close();
			return survivorList;
		} catch (Exception e) {
			tx.rollback();
			return null;
		}
	}

	public boolean deleteEntity(long id) throws Exception {
		try {
			session = sessionFactory.openSession();
			Object o = session.load(Survivor.class, id);
			tx = session.getTransaction();
			session.beginTransaction();
			session.delete(o);
			tx.commit();
			return false;
		} catch (Exception e) {
			tx.rollback();
			return true;
		}
	}
}
