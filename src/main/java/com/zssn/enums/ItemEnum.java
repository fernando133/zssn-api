package com.zssn.enums;

/**
 * 
 * @author fernandogualberto
 *
 */
public class ItemEnum {

	public ItemEnum() {}
	
	public enum Description {

		WATER(1, "Water", 4),
		FOOD(2, "Food", 3),
		MEDICATION(3, "Medication", 2),
		AMMUNITION(4,"Ammunition", 1);

		private long id;
		private String name;
		private int points;

		Description(long id, String name, int points) {
			this.id = id;
			this.name = name;
			this.points = points;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getPoints() {
			return points;
		}

		public void setPoints(int points) {
			this.points = points;
		}

	}
}
