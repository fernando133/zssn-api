create database zssn_db;
use zssn_db;

create table survivor (
	id 	bigint not null primary key auto_increment,
    survivorName varchar(50),
    age int, 
    gender varchar(6),
    lat double,
    lon double,
    timesMarkedAsInfected int,
    infected int,
    createdAt datetime,
    updatedAt datetime 
);

create table item (
	id bigint not null primary key auto_increment,
    name varchar(30),
    points int
);

create table inventory (
	id bigint not null primary key auto_increment,
    idSurvivor bigint,
    idItem bigint not null,
    amount int not null,
    
    foreign key (idSurvivor) references survivor(id),
    foreign key (idItem) references item(id)
);

insert into item (name, points) values ('Water', 4);
insert into item (name, points) values ('Food', 3);
insert into item (name, points) values ('Medication', 2);
insert into item (name, points) values ('Ammunition', 1);